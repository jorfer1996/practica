﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Practica.Models
{
    public class InvoiceDetailViewModel
    {
        public decimal Total { get; set; }

        public decimal SellPrice { get; set; }

        public string Name { get; set; }

        public decimal UnitPrice { get; set; }


    }
}

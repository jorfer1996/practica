﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Practica.Data.Repositories
{
    public interface IGenericFachada <T> where T : class
    {

        IQueryable<T> GetAll();

        Task<T> GetByIdAsync(int id);
        
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Practica.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Practica.Data.Repositories
{
    public class ClientFachada : GenericFachada<Client>, IClientFachada
    {
       private readonly PracticaContext context;

        public ClientFachada(PracticaContext context) : base(context)
        {
            this.context = context;
        }

        public IEnumerable<Invoice> GetAllInvoice(int id)
        {
            return from c in context.Invoice
                            where
                            c.ClientId == id
                            select c;
        }

        public IEnumerable<InvoiceDetailViewModel> GetAllInvoiceDetail(int? id)
        {
            var consult = from invDet in context.InvoiceDetail
                            from prod in context.Product
                            where invDet.InvoiceId == id
                            where invDet.ProductId == prod.Id
                            select new
                            {
                                invDet.Total,
                                invDet.SellPrice,
                                prod.Name,
                                prod.UnitPrice,
                                
                            };


            List<InvoiceDetailViewModel> invoiceViewModels = new List<InvoiceDetailViewModel>();
            decimal priceTotal = 0;
            foreach (var obj in consult.ToList())
            {
                priceTotal += (decimal)obj.Total;
                invoiceViewModels.Add(
                    new InvoiceDetailViewModel
                    {
                        Name = obj.Name,
                        UnitPrice = obj.UnitPrice,
                        Total = (decimal)obj.Total,
                        SellPrice = obj.SellPrice
                    }
                    );
            }
            return invoiceViewModels;
        }

        

    }


}

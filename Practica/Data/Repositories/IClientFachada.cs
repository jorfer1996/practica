﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Practica.Models;

namespace Practica.Data.Repositories
{
    public interface IClientFachada : IGenericFachada<Client>
    {

       IEnumerable<Invoice> GetAllInvoice(int id); //devuelve la consulta no materializada
       IEnumerable<InvoiceDetailViewModel> GetAllInvoiceDetail(int? id);
    }
}

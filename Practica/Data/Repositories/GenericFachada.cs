﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Practica.Data.Entities;

namespace Practica.Data.Repositories
{
    public class GenericFachada<T> : IGenericFachada<T> where T : class , IEntity
    {

        private readonly PracticaContext context;

        public GenericFachada(PracticaContext context)
        {
            this.context = context;
        }


        public IQueryable<T> GetAll()
        {
            //AsNoTracking(): sirve para que funcione el metodo regenerico
            return this.context.Set<T>().AsNoTracking();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await this.context.Set<T>().AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);
        }


    }
}

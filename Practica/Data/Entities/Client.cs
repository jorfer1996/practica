﻿using Practica.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Practica.Data
{
    public partial class Client : IEntity
    {
        public Client()
        {
            Invoice = new HashSet<Invoice>();
        }

        [Column("ClientId")]
        public int Id { get; set; }

        [MaxLength(50, ErrorMessage = "The field {0} only can contain {1} characters length")]
        public string Name { get; set; }

        public string Address { get; set; }

        public DateTime BornDate { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public virtual ICollection<Invoice> Invoice { get; set; }
    }
}

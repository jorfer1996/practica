﻿using Practica.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Practica.Data
{
    public partial class Product : IEntity
    {
        public Product()
        {
            InvoiceDetail = new HashSet<InvoiceDetail>();
        }

        [Column("ProductId")]
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal UnitPrice { get; set; }

        public virtual ICollection<InvoiceDetail> InvoiceDetail { get; set; }
    }
}

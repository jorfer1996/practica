﻿using Practica.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Practica.Data
{
    public partial class Invoice : IEntity
    {
        public Invoice()
        {
            InvoiceDetail = new HashSet<InvoiceDetail>();
        }

        [Column("InvoiceId")]
        public int Id { get; set; }

        public int ClientId { get; set; }

        public DateTime IssueDate { get; set; }

        public bool Paid { get; set; }

        public virtual Client Client { get; set; }

        public virtual ICollection<InvoiceDetail> InvoiceDetail { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Practica.Data;
using Practica.Data.Repositories;

namespace Practica.Controllers
{
    public class ClientsController : Controller
    {
        private readonly IClientFachada clientFachada;

        public ClientsController(IClientFachada clientFachada)
        {
            this.clientFachada = clientFachada;
        }

        // GET: Clients
        public IActionResult Index()
        {
            return View(clientFachada.GetAll().OrderBy(p => p.Name));
        }

        // GET: Clients/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            return View(clientFachada.GetAllInvoice(id.Value));
        }


        public IActionResult DetailsInvoice(int? id)
        {
            if (id == null)
                return NotFound();

            return View(clientFachada.GetAllInvoiceDetail(id));
        }

        [HttpPost]
        public IActionResult Create()
        {
            return NotFound();
        }

      
        


  
    }
}
